# 1 - Crie um fork deste repositóro

# 2 - No seu fork, adicione uma pasta com o seu nome e sobrenome e dentro da pasta coloque o seu currículo. Se preferir, pode colocar apenas o link do seu LinkedIn. Vale observar que o repositório é público e as informações podem ser visualizadas por outras pessoas.

# 3 - Crie um merge request enviando os ajustes que você efetuou no seu fork (pasta com o currículo) para o repositório original (g3-solutions-estagio/devops)

# 4 - Boa sorte! 